import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeRoutingModule } from './home-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AngularMaterialModule } from '../angular-material/angular-material/angular-material.module';
import { HttpClientModule } from '@angular/common/http';
import { InvoicePayComponent } from './invoice-pay/invoice-pay.component';
import { ClientComponent } from './client/client.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { PaymentComponent } from './payment/payment.component';
import { ProjectComponent } from './project/project.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [DashboardComponent, InvoicePayComponent, ClientComponent, InvoiceComponent, PaymentComponent, ProjectComponent],
  imports: [
    CommonModule,
    AngularMaterialModule,
    HttpClientModule,
    HomeRoutingModule,
    ReactiveFormsModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],

})
export class HomeModule { }
