import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainLayoutComponent } from './main-layout/main-layout.component';
import { RouterModule } from '@angular/router';
import { AngularMaterialModule } from '../angular-material/angular-material/angular-material.module';

@NgModule({
  declarations: [MainLayoutComponent],
  imports: [
    CommonModule,
    AngularMaterialModule,
    RouterModule.forChild([]),
    

  ],
  exports: [
    MainLayoutComponent,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],

})
export class LoaderModule { }
