import { HttpClient, HttpParams } from "@angular/common/http";

export class DataService {
  
  constructor(private _url: string, private _http: HttpClient) { }

  list(params?:any) {
    // if(params){
    if(params && Object.keys(params).length>0){
      const httpParams = new HttpParams({ fromObject: params });
      return this._http
        .get(this._url, { params:httpParams })
        .toPromise();
    }
    // }
    else {
      return this._http
        .get(this._url)
        .toPromise();
      }
  }
  read(id: string) {
    const url = this._url + "/" + id;
    return this._http
      .get(url, {})
      .toPromise()
  }
  delete(id: string) {
    const url = this._url + "/" + id;
    return this._http
      .delete(url)
      .toPromise()
  }
  create(data:any) {
    const url = this._url;
    return this._http
      .post(url, data)
      .toPromise()
  }
  update(data:any) {
    const url = this._url + "/" + data.id;
    return this._http
      .put(url, data)
      .toPromise()
  }
  partial(data:any) {
    const url = this._url + "/" + data.id;
    return this._http
      .patch(url, data)
      .toPromise()
  }
}
