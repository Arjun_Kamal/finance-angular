import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { DataService } from './data.service';

@Injectable({
  providedIn: 'root'
})
export class ClientApiService extends DataService {

  constructor(http: HttpClient, @Inject("baseUrl") private baseUrl:string) {
    super(baseUrl + "/client", http);
  }
}