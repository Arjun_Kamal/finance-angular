import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/_service/auth.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
  
  constructor(private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private authService: AuthService) { }

  loginForm: FormGroup = this.fb.group({
    username: new FormControl('',[Validators.required]),
    password: new FormControl('', [Validators.required]),
  });
  returnUrl: string = "";
  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe(params => {
      this.returnUrl = params["returnUrl"]
    })
    console.log()

  }
  signin() {
    if(this.loginForm.status != 'INVALID') {
      this.authService.login(this.loginForm.value, this.returnUrl);
    }
    this.loginForm.markAllAsTouched();
  }
}
