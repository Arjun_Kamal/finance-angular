const providers: any[] = [
  { provide: "environment", useValue: "Production"},
  { provide: "baseUrl", useValue: "http://localhost:8000"},
]

export const ENV_PROVIDERS = providers;

export const environment = {
  production: true,
  api_url: "http://localhost:8000",
};
