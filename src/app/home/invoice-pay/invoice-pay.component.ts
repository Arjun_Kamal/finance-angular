import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { InvoiceApiService } from 'src/app/_service/invoice-api.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { PaymentApiService } from 'src/app/_service/payment-api.service';

@Component({
  selector: 'app-invoice-pay',
  templateUrl: './invoice-pay.component.html',
  styleUrls: ['./invoice-pay.component.css']
})
export class InvoicePayComponent implements OnInit {
  invoice_details: any;
  isLoading: boolean = false;
  paymentHandler:any = null;
  constructor(private activatedRoute: ActivatedRoute,
    private _snackBar: MatSnackBar,
    private invoiceApiService: InvoiceApiService) { }

  ngOnInit(): void {
    this.invokeStripe();
    this.activatedRoute.params.subscribe(async params => {
      const id = params["id"];
      this.isLoading = true;
      this.invoiceApiService.read(id).then(res => {
        this.invoice_details = res;
        this.isLoading = false;
      })
      .catch(e => {
        this.isLoading = false;
      });
    })
    
  }
  async makePayment(amount: number, currency: string, email: string, id: number) {
    const stripeToken = await this.waitMakePayment(amount, currency, email);
    if(stripeToken){
      this.invoiceApiService.partial({status: 'success', transaction: stripeToken, id: id}).then(res =>{
        this.invoice_details = res;
      })
      this.openSnackBar('Payment has been successfull!');
    }
  }

  async waitMakePayment(amount: number, currency: string, email: string) {
    return new Promise(resolve => {
      const paymentHandler = (<any>window).StripeCheckout.configure({
        key: 'pk_test_51JUF3xSBhOc9BI4mQY7LqSfObyMQAN5cwaoqcnX8RyYqboPGDZj1YmIbB4o2wWUSS2N109txBk188ZQZaxSmzuCB00Fmzl1USU',
        locale: 'auto',
        email: email, 
        currency: currency,
        token: function (stripeToken: any) {
          return resolve(stripeToken)
        }
      
    })
    paymentHandler.open({
      name: 'Finanz',
      description: 'Pay Invoice via Stripe',
      amount: amount * 100
    });
  })}  
  
  invokeStripe() {
    if(!window.document.getElementById('stripe-script')) {
      const script = window.document.createElement("script");
      script.id = "stripe-script";
      script.type = "text/javascript";
      script.src = "https://checkout.stripe.com/checkout.js";
      script.onload = () => {
        this.paymentHandler = (<any>window).StripeCheckout.configure({
          key: 'pk_test_51JUF3xSBhOc9BI4mQY7LqSfObyMQAN5cwaoqcnX8RyYqboPGDZj1YmIbB4o2wWUSS2N109txBk188ZQZaxSmzuCB00Fmzl1USU',
          locale: 'auto',
          token: function (stripeToken: any) {
            this.openSnackBar('Payment has been successfull!');
          }
        });
      }
        
      window.document.body.appendChild(script);
    }
  }
  openSnackBar(message: string, action: string="", duration: number = 3000) {
    this._snackBar.open(message, action, {
      duration: duration,
    });
  };


}
