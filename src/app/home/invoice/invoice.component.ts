import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ClientApiService } from 'src/app/_service/client-api.service';
import { InvoiceApiService } from 'src/app/_service/invoice-api.service';
import { PaymentApiService } from 'src/app/_service/payment-api.service';
import { ProjectApiService } from 'src/app/_service/project-api.service';

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.css']
})
export class InvoiceComponent implements OnInit {
  clients:any = [];
  projects:any = [];
  payments:any = [];
  savedDetails: any = {};
  currency: any = ['INR','SGD','USD'];
  constructor(private fb: FormBuilder,
    private _snackBar: MatSnackBar,
    private clientApiService: ClientApiService,
    private paymentApiService: PaymentApiService,
    private invoiceApiService: InvoiceApiService,
    private projectApiService: ProjectApiService
    ) { }
  
  invoiceForm: FormGroup = this.fb.group({
    invoice_id: new FormControl('',[Validators.required]),
    client_id: new FormControl('', [Validators.required]),
    project_id: new FormControl('', [Validators.required]),
    currency: new FormControl('', [Validators.required]),
    amount: new FormControl(0, [Validators.required]),
  });

  ngOnInit(): void {
    this.savedDetails = {};
    this.clientApiService.list().then(res => {
      this.clients = res;
    })
    this.paymentApiService.list().then(res => {
      this.payments = res;
    })
    this.projectApiService.list().then(res => {
      this.projects = res;
    })

  }
  save(){
    this.savedDetails = {};
    if(this.invoiceForm.status != 'INVALID'){
      this.invoiceApiService.create(this.invoiceForm.value).then(res => {
        this.savedDetails = <any>res;
        this.openSnackBar("Saved successfully.")
      })
    }
    this.invoiceForm.markAllAsTouched();

  }
  openSnackBar(message: string, action: string="", duration: number = 3000) {
    this._snackBar.open(message, action, {
      duration: duration,
    });
  };

}
