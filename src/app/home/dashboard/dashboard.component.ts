import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/_service/auth.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  userDetails: any;
  constructor(private authService: AuthService) { }

  async ngOnInit(): Promise<void> {
    this.authService.isUser().subscribe(e=> {
      this.userDetails = e;
    });
  }
}
