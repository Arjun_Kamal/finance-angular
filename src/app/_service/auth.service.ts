import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient,
    private router: Router) { }

  isLoginSubject = new BehaviorSubject<boolean>(this.hasToken());
  isUserSubject = new BehaviorSubject<{}>({});

  private hasToken() : boolean {
    return !!localStorage.getItem('token');
  }

  login(user:any, returnUrl: string) {
    localStorage.clear();
    return this.http
    .post( environment.api_url+'/auth/token/', user)
    .toPromise()
    .then(response => {
      let token: any = response;
      localStorage.setItem('token', token.token);
      this.isLoginSubject.next(true);
      this.me();
      this.router.navigate([returnUrl?returnUrl:"/dashboard"]);
    });
  }
  logout() : void {
    localStorage.removeItem('token');
    this.isLoginSubject.next(false);
    this.isUserSubject.next({})
  }

  isLoggedIn() : Observable<boolean> {
    return this.isLoginSubject.asObservable();
  }
  me() {
    return this.http
    .get( environment.api_url+'/me/')
    .toPromise()
    .then(res => {
      this.isUserSubject.next(res);
    })
  }
  getUser() {
    return this.isUserSubject.getValue();
  }
  getToken() {
    return this.isLoginSubject.getValue();
  }
  isUser() : Observable<{}> {
    return this.isUserSubject.asObservable();
  }
}
