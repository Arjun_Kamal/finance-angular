import { Component, OnInit } from '@angular/core';
import { AuthService } from './_service/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  constructor(private authService: AuthService) {}

  async ngOnInit(): Promise<void> {
    if(Object.keys(this.authService.getUser()).length === 0 && this.authService.getToken()) {
      await this.updateUserDetails();
    }
  }

  async updateUserDetails(){
    return new Promise(resolve => {
      this.authService.me();
      return resolve("");
    });
  }
  
}
