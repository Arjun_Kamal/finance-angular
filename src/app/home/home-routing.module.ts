import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainLayoutComponent } from '../loader/main-layout/main-layout.component';
import { AuthGuard } from '../_guard/auth.guard';
import { ClientComponent } from './client/client.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { InvoicePayComponent } from './invoice-pay/invoice-pay.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { PaymentComponent } from './payment/payment.component';
import { ProjectComponent } from './project/project.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  },
  { 
    path: 'dashboard',
    component: MainLayoutComponent,  
    children: [
      { 
        path: '', 
        component: DashboardComponent,
      }
    ],
    canActivate: [AuthGuard],
  },
  { 
    path: 'payment',
    component: PaymentComponent,  
    children: [
      { 
        path: '', 
        component: DashboardComponent,
      }
    ],
    canActivate: [AuthGuard],
  },
  { 
    path: 'client',
    component: MainLayoutComponent,  
    children: [
      { 
        path: '', 
        component: ClientComponent,
      }
    ],
    canActivate: [AuthGuard],
  },
  { 
    path: 'project',
    component: MainLayoutComponent,  
    children: [
      { 
        path: '', 
        component: ProjectComponent,
      }
    ],
    canActivate: [AuthGuard],
  },
  { 
    path: 'invoice',
    component: MainLayoutComponent,  
    children: [
      { 
        path: '', 
        component: InvoiceComponent,
      },
      { 
        path: ':id', 
        component: InvoicePayComponent,
      }
    ],
    canActivate: [AuthGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
