import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpHeaders
} from '@angular/common/http';
import { from, Observable } from 'rxjs';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor() {}
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    
    return from(this.handleAccess(request, next));
    }
    
  private async handleAccess(request: HttpRequest<any>, next: HttpHandler):
  Promise<HttpEvent<any>>{
    const token = localStorage.getItem("token");
    if(token){
      request = request.clone({ 
        headers: new HttpHeaders({'Authorization':`Token ${token}`})
      });
    }
    
    return next.handle(request).toPromise();
  }
}
