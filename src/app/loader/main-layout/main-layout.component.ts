import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/_service/auth.service';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.css']
})
export class MainLayoutComponent implements OnInit {
  userDetails: any;
  constructor(private authService: AuthService) { }

  ngOnInit(): void {
    this.authService.isUser().subscribe(e=> {
      this.userDetails = e;
    });
  }

}
