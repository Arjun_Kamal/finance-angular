import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { DataService } from './data.service';

@Injectable({
  providedIn: 'root'
})
export class InvoiceApiService extends DataService {

  constructor(http: HttpClient, @Inject("baseUrl") private baseUrl:string) {
    super(baseUrl + "/invoice", http);
   }
}
