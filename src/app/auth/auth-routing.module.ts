import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainLayoutComponent } from 'src/app/loader/main-layout/main-layout.component';
import { AuthGuard } from '../_guard/auth.guard';
import { SigninComponent } from './signin/signin.component';
import { SignoutComponent } from './signout/signout.component';


const routes: Routes = [
  { 
    path: 'signin',
    component: MainLayoutComponent,  
    children: [
      { 
        path: '', 
        component: SigninComponent,
      }
    ],
  },
  { 
    path: 'signout',
    component: SignoutComponent,  
    children: [
      { 
        path: '', 
        component: SigninComponent,
      }
    ],
    canActivate: [AuthGuard],
  },
  {
    path:'',
    loadChildren: () =>  import('../home/home.module').then(mod => mod.HomeModule),
    canActivate: [AuthGuard],
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
